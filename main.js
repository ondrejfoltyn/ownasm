/*
 * Excuse the messy code, it was kind of a speedrun :)
 */

window.onbeforeunload = function() {
    if(!autosaveEnabled)
        return 'Do you really want to leave this page?'
};

var outputElem = document.getElementById("txt-output")
var code = ""

var header = {}
var commands = []

var compiledWords = []
var anchors = {}

var macros = {}

var autoassembleEnabled = true
var autosaveEnabled = true

function ToggleAutoassemble() {
    let btn = document.getElementById("btn-autoassemble")
    if(autoassembleEnabled) {
        autoassembleEnabled = false
        btn.innerHTML = "Auto-assemble [Disabled]"
    } else {
        autoassembleEnabled = true
        btn.innerHTML = "Auto-assemble [Enabled]"
    }
}

function ToggleAutosave() {
    let btn = document.getElementById("btn-autosave")
    if(autosaveEnabled) {
        autosaveEnabled = false
        btn.innerHTML = "Auto-save [Disabled]"
    } else {
        autosaveEnabled = true
        btn.innerHTML = "Auto-save [Enabled]"
    }
}

window.onload = () => {
    if(localStorage["autosave"] != undefined)
        if(confirm("Load autosave?"))
            editor.setValue(localStorage["autosave"])

    if(localStorage["autosaveEnabled"])
        autosaveEnabled = true
    else if(localStorage["autosaveEnabled"] == false)
        autosaveEnabled = false

    if(localStorage["autoassembleEnabled"])
        autoassembleEnabled = true
    else if(localStorage["autoassembleEnabled"] == false)
        autoassembleEnabled = false

    setInterval(() => {
        localStorage["autoassembleEnabled"] = autoassembleEnabled
        localStorage["autosaveEnabled"] = autosaveEnabled
        if(autosaveEnabled)
            localStorage["autosave"] = editor.getValue()
        try {
            if(autoassembleEnabled)
                Assemble()
        } catch { }
    }, 1000)
}

function Assemble() {
    code = editor.getValue()

    outputElem.innerText = ""
    header = {}
    commands = []
    compiledWords = []
    anchors = {}
    macros = {}

    if(code[0] != '{') {
        outputElem.innerText += "Error, header data not found."
        return
    }
    let matches = code.match(/\{(.*?)\}(.*)/s)
    code = matches[2]

    ProcessHeader(matches[1]);
    //load preferences
    let preferenceFormat = document.getElementById("sel-format").value
    if(preferenceFormat != "default")
        header["format"] = preferenceFormat
    
    let prepared_code = ProcessMacros()
    AssembleCode(prepared_code)
    FormatCode()
}

function ProcessMacros() {
    let processed_code = ""

    let lines = code.split('\n')

    let readingMacro = false
    let macro = {}

    for(let i = 0; i < lines.length; i++) {
        let line = lines[i].trim()

        if(line == '')
            continue
        
        if(line.indexOf("%macro") == 0) {
            macro = {}
            readingMacro = true
            macro["commands"] = []
            macro["name"] = line.split(' ')[1]
        } else if(line.indexOf("%endmacro") == 0) {
            readingMacro = false
            macros[macro["name"]] = macro
        } else if(line[0] == "%") {
            lineSpl = line.split(' ')
            // fill in the macro
            try {
                let macro = macros[lineSpl[0].substr(1)]
                let params = lineSpl.slice(1)
                for(let x = 0; x < macro["commands"].length; x++) {
                    let commandSpl = macro["commands"][x].split(' ')
                    for(let y = 0; y < commandSpl.length; y++) {
                        let part = commandSpl[y]
                        if(part[0] != "$")
                            processed_code += part + " "
                        else {
                            processed_code += params[parseInt(part.substr(1))-1]
                        }
                    }
                    processed_code += "\n"
                }
            } catch(err) {
                outputElem.innerHTML += "Macro processing failed:\n" + line + "\n^^^ -  Cannot fill in the macro."
                throw new Error('macro failed',err)
            }
        } else if(readingMacro) {
            macro["commands"].push(line)
        } else {
            processed_code += line + "\n"
        }
    }
    return processed_code
}

function FormatCode() {
    if(compiledWords.length <= 0) {
        outputElem.innerHTML = "Nothing to compile!"
        return
    }
    if(header['format'] == "pretty-hex") {
        for(let i = 0; i < compiledWords.length; i++) {
            outputElem.innerHTML += i.toString(16).padStart(16/4) + " | "
            outputElem.innerHTML += compiledWords[i].compiled.substr(0,header['command_len']/4) + " " + compiledWords[i].compiled.substr(header['command_len']/4) + " | "
            outputElem.innerHTML += compiledWords[i].line
            outputElem.innerHTML += "\n"
        }
    } else if(header['format'] == "hex") {
        for(let i = 0; i < compiledWords.length; i++) {
            outputElem.innerHTML += compiledWords[i].compiled.padStart(header['word_len']/4,'0')
        }
    } else if(header['format'] == "logisim-hex") {
        outputElem.innerHTML += "v2.0 raw\n"
        for(let i = 0; i < compiledWords.length; i++) {
            outputElem.innerHTML += parseInt(compiledWords[i].compiled.padStart(header['word_len']/4),16).toString(16) + " "
        }
    } else if(header['format'] == "debug-json") {
        let output = {"header":header, "anchors":anchors, "compiledWords":compiledWords}
        outputElem.innerHTML += JSON.stringify(output)
    } else {
        outputElem.innerHTML += "Unknown code format, check 'format' header!"
    }
}

function AssembleCode(prepared_code) {
    let lines = prepared_code.split('\n')

    for(let i = 0; i < lines.length; i++) {
        let line = lines[i].trim()

        //remove comments
        if(line.indexOf(';') > -1)
            line = lines[i].match(/(.*?)\;/)[1].trim()
        else
            line = lines[i].trim()

        if(line == '')
            continue

        //assemble
        if(line[0] == ".") {
            // anchor
            anchors[line.substr(1)] = compiledWords.length
        } else {
            // find command
            let cmd = line.split(' ')[0].trim()
            let found = false
            for(let y = 0; y < commands.length; y++) {
                if(commands[y]['command'].split(' ')[0] == cmd) {
                    found = true

                    let cmdTemplate = commands[y]

                    let compiledHex = ""
                    compiledHex += cmdTemplate['opcode'].toString(16).padStart(header['command_len']/4,'0')

                    let templateSpl = cmdTemplate['command'].split(' ')
                    let commandSpl = line.split(' ')
                    
                    // process params
                    for(let x = 0; x < templateSpl.length; x++) {
                        let templateParam = templateSpl[x]
                        let commandParam = commandSpl[x]
                        if(commandParam[0] == ".") {
                            //its an anchor
                            try {
                                commandParam = "0x" + anchors[commandParam.substr(1)].toString(16)
                            } catch(err) {
                                //probably will be defined in future
                                commandParam = "PROMISE_" + commandParam
                            }
                        } else if(commandParam[0] == "+") {
                            //address increment
                            commandParam = "0x" + (compiledWords.length + parseInt(commandParam.substr(1))).toString(16)
                        } else if(commandParam[0] == "-") {
                            //address decrement
                            commandParam = "0x" + (compiledWords.length - parseInt(commandParam.substr(1))).toString(16)
                            compiledHex += parseInt(commandParam).toString(16).padStart(cmdTemplate['params'][templateParam]/4,'0')
                        }
                        if(templateParam[0] == '<') {
                            // its parameter
                            templateParam = templateParam.substr(1,templateParam.length-2)

                            if(commandParam.startsWith("PROMISE_")) {
                                console.debug("Promise detected: " + commandParam);
                                compiledHex += commandParam
                            } else if(isNaN(parseInt(commandParam))) {
                                outputElem.innerHTML += "Compilation failed:\n" + line + "\n^^^ -  Cannot parse value to number. ("+commandParam+")"
                                throw new Error('compilation failed')
                            } else {
                                compiledHex += parseInt(commandParam).toString(16).padStart(cmdTemplate['params'][templateParam]/4,'0')
                            }
                        } else {
                            // its continuation of command
                            if(commandParam != templateParam) {
                                found = false
                                break
                            } else {
                                continue
                            }
                        }
                    }
                    if(found) {
                        compiledWords.push({'compiled':compiledHex.padStart(header['word_len']/4,'0'),'line':line})
                        break;
                    } else
                        continue
                }
            }
            if(!found){
                outputElem.innerHTML += "Compilation failed:\n" + line + "\n^^^ - Instruction not found"
                throw new Error('compilation failed')
            }
        }
    }
    
    //walk through commands again to fill in future promises
    for(let i = 0; i < compiledWords.length; i++) {
        let lineHex = compiledWords[i]['compiled']
        let commandHex = lineHex.substr(0,header['command_len']/4)
        let param = lineHex.substr(header['command_len']/4)
        if(param.startsWith('PROMISE_')) {
            param = param.substr('PROMISE_'.length)
            try {
                param = "0x" + anchors[param.substr(1)].toString(16)
                lineHex = commandHex + parseInt(param).toString(16).padStart((header['word_len']-header['command_len'])/4,'0')
                compiledWords[i]['compiled'] = lineHex
            } catch(err) {
                outputElem.innerHTML += "Compilation failed:\n" + param + "\n^^^ - Promised anchor not defined."
                throw new Error('compilation failed',err)
            }
        }
    }
}

function ProcessHeader(headerString) {
    let lines = headerString.split('\n')
    for(let i = 0; i < lines.length; i++) {
        let line = lines[i].trim()

        //remove comments
        if(line.indexOf(';') > -1)
            line = lines[i].match(/(.*?)\;/)[1].trim()
        else
            line = lines[i].trim()

        if(line == "")
            continue
        if(line[0] == "#") {
            // headers
            let spl = line.split(':')
            if(spl.length < 2) {
                outputElem.innerHTML += "Cannot process headers:\n" + line + "\n^^^ - Invalid format"
                throw new Error('cannot process headers')
            }
            header[spl[0].trim().substr(1)] = spl[1].trim()
        } else {
            // commands
            spl = line.split('#')
            if(spl.length < 2) {
                outputElem.innerHTML += "Cannot process command template:\n" + line + "\n^^^ - Invalid format"
                throw new Error('cannot process headers')
            }
            let cmd = {}
            cmd['command'] = spl[0].trim()
            cmd['params'] = {}
            let paramSpl = spl[1].trim().split(' ')
            for(let y = 0; y < paramSpl.length; y++) {
                let param = paramSpl[y]
                if(y == 0) {
                        cmd['opcode'] = parseInt(param)
                    if(isNaN(parseInt(param))) {
                        outputElem.innerHTML += "Cannot process command template:\n" + line + "\n^^^ - Cannot parse opcode to number."
                        throw new Error('cannot process headers')
                    }
                } else if(param.indexOf(':') > -1) {
                    let spl2 = param.split(':')
                    if(spl2.length < 2) {
                        outputElem.innerHTML += "Cannot process command template:\n" + line + "\n^^^ - Invalid format."
                        throw new Error('cannot process headers')
                    }
                    cmd['params'][spl2[0]] = parseInt(spl2[1])
                    if(isNaN(parseInt(spl2[1]))) {
                        outputElem.innerHTML += "Cannot process command template:\n" + line + "\n^^^ - Cannot parse bit length."
                        throw new Error('cannot process headers',err)
                    }
                }
            }
            commands.push(cmd)
        }
    }
}

